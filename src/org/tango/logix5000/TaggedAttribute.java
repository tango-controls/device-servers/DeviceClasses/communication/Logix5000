package org.tango.logix5000;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbAttribute;
import fr.esrf.TangoDs.Except;
import fr.esrf.TangoDs.TangoConst;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributePropertiesImpl;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import se.opendataexchange.ethernetip4j.EthernetIPException;
import se.opendataexchange.ethernetip4j.clx.SimpleLogixCommunicator;

public class TaggedAttribute implements IAttributeBehavior {

  private SimpleLogixCommunicator plc;
  private String name;         // Tango attribute name
  private String tagName;      // TAG name
  private int size;            // Array size
  private int type;            // Data type
  private boolean isReadWrite; // RW attribute
  private boolean isExpert;    // Display level
  private double factor;       // Multiplicative factor
  private double startOffset;  // For resetable attribute (initial value)
  private String resetCommand; // For resetable attribute (name of command)
  private long   resetDate;    // For resetable attribute (date of last reset)

  private String devName;
  private String format;
  private String unit;


  static Database db;

  public TaggedAttribute(String deviceName,SimpleLogixCommunicator plc,String attDef) throws DevFailed {

    this.plc = plc;
    size = 1;
    isExpert = false;
    factor = 1.0;
    startOffset = 0;
    db = null;
    resetCommand = "";
    resetDate = 0;
    devName = deviceName;
    unit = "";
    format = "%.0f";

    String[] fields = attDef.trim().split(",");

    if(fields.length<2) {
      Except.throw_exception(
          "error_init",
          "Invalid item configuration for "+attDef+": Type,Tag_name[,length][,name][,level][,factor][,resetcommand] expected ",
          "TaggedAttribute()");
    }

    isReadWrite = (fields[0].startsWith("RW"));
    String typeStr;
    if(isReadWrite) {
      typeStr = fields[0].substring(2);
    } else {
      typeStr = fields[0];
    }

    if(typeStr.equalsIgnoreCase("Boolean")) {
      // BOOL
      type = TangoConst.Tango_DEV_BOOLEAN;
    } else if (typeStr.equalsIgnoreCase("Byte")) {
      // SINT
      type = TangoConst.Tango_DEV_UCHAR;
    } else if (typeStr.equalsIgnoreCase("Short")) {
      // INT
      type = TangoConst.Tango_DEV_SHORT;
    } else if (typeStr.equalsIgnoreCase("Int")) {
      // DINT
      type = TangoConst.Tango_DEV_LONG;
    } else if (typeStr.equalsIgnoreCase("Double")) {
      // REAL
      type = TangoConst.Tango_DEV_DOUBLE;
      format = "%.3f"; // Default format for double
    } else {
      Except.throw_exception(
          "error_init",
          "Invalid item configuration for "+attDef+": Unknown or unsupported data type",
          "TaggedAttribute()");
    }

    if(fields.length>=3) {
      try {
        size = Integer.parseInt(fields[2]);
      } catch (NumberFormatException e) {
        Except.throw_exception(
            "error_init",
            "Invalid item configuration for "+attDef+": Invalid length number",
            "TaggedAttribute()");
      }
    }

    if(fields.length>=4) {
      name = fields[3];
    } else {
      name = fields[1]; // Attribute name as tag nae
    }

    if(fields.length>=5) {
      isExpert = fields[4].equalsIgnoreCase("expert");
    }

    if(fields.length>=6) {
      factor = Double.parseDouble(fields[5]);
    }

    if(fields.length>=7) {
      resetCommand = fields[6];
    }

    if(isReadWrite && size>1) {
      Except.throw_exception(
          "error_init",
          "Invalid item configuration for "+attDef+": Writable attribute must be scalar",
          "TaggedAttribute()");
    }

    tagName = fields[1];
    if(tagName.endsWith("[0]"))
      tagName = tagName.substring(0,tagName.length()-3);


  }

  public boolean isOperator() {
    return !isExpert;
  }

  public String getResetCommand() {
    return resetCommand;
  }

  public long getResetDate() {
    return resetDate;
  }

  public String getValueAsStr() {
    try {
      if( type == TangoConst.Tango_DEV_BOOLEAN ) {
        boolean b = getValueAsDouble()==1.0;
        return name + ": " + Boolean.toString(b);
      } else {
        return name + ": " + String.format(format, getValueAsDouble()) + " " + unit;
      }
    } catch (DevFailed e) {
      return "--.--- " + unit;
    }
  }

  public void reset() throws DevFailed {

    startOffset = 0;
    startOffset = getValueAsDouble();
    resetDate = System.currentTimeMillis() / 1000;
    // Save to DB
    if(db==null) db = ApiUtil.get_db_obj();
    DbAttribute da = new DbAttribute(name);
    da.add("LastValue",startOffset);
    String resetDateStr = String.format("%d",resetDate);
    da.add("ResetDate",resetDateStr);
    db.put_device_attribute_property(devName,da);

  }

  public void initResetAttribute() {

    try {
      if (db == null) db = ApiUtil.get_db_obj();
      DbAttribute da = db.get_device_attribute_property(devName,name);

      if(!da.isEmpty()) {
        if(!da.is_empty("LastValue")) {
          String[] startOffsetStr = da.get_value("LastValue");
          if (startOffsetStr.length == 1 && !startOffsetStr[0].isEmpty())
            startOffset = Double.parseDouble(startOffsetStr[0]);
        }

        if(!da.is_empty("ResetDate")) {
          String[] resetDateStr = da.get_value("ResetDate");
          if (resetDateStr.length == 1 && !resetDateStr[0].isEmpty())
            resetDate = Long.parseLong(resetDateStr[0]);
        }
      }

    } catch (DevFailed e) {}

  }

  @Override
  public AttributeConfiguration getConfiguration() throws DevFailed {

    AttributeConfiguration cfg = new AttributeConfiguration();
    cfg.setName(name);

    AttrDataFormat format;

    if(size==1) {
      format = AttrDataFormat.SCALAR;
    } else {
      format = AttrDataFormat.SPECTRUM;
    }

    if(isReadWrite)
      cfg.setWritable(AttrWriteType.READ_WRITE);

    cfg.setTangoType(type, format);

    if(isExpert)
      cfg.setDispLevel(DispLevel.EXPERT);

    AttributePropertiesImpl attCfg = cfg.getAttributeProperties();

    if (db == null) db = ApiUtil.get_db_obj();
    DbAttribute da = db.get_device_attribute_property(devName,name);

    // If no label defined, set the attribute name as label
    if(!da.isEmpty() && da.is_empty("label"))
      attCfg.setLabel(name);

    cfg.setAttributeProperties(attCfg);

    // Get default format and unit
    if(!da.isEmpty() && !da.is_empty("format"))
      this.format = da.get_value("format")[0];

    if(!da.isEmpty() && !da.is_empty("unit"))
      this.unit = da.get_value("unit")[0];

    return cfg;

  }

  private Object[] getValueFromHardware() throws DevFailed {

    if(plc==null) {
      Except.throw_exception(
              "error_read",
              "Not connected to PLC",
              "TaggedAttribute.getValue()"
      );
    }

    Object[] value = null;

    try {
      value = (Object[])plc.read(tagName,size);
    } catch (EthernetIPException e) {
      Except.throw_exception(
              "error_read",
              "Reading error " + e.getMessage(),
              "TaggedAttribute.getValue()"
      );
    }

    return value;

  }

  @Override
  public AttributeValue getValue() throws DevFailed {

    AttributeValue ret = new AttributeValue();
    Object[] value = getValueFromHardware();

    switch (type) {

      case TangoConst.Tango_DEV_BOOLEAN:
        if(size==1) {
          ret.setValue(getValueAsBooleanArray(value)[0]);
        } else {
          ret.setValue(getValueAsBooleanArray(value));
        }
        break;

      case TangoConst.Tango_DEV_UCHAR:
        if(size==1) {
          ret.setValue(getValueAsCharArray(value)[0]);
        } else {
          ret.setValue(getValueAsCharArray(value));
        }
        break;

      case TangoConst.Tango_DEV_SHORT:
        if(size==1) {
          ret.setValue(getValueAsShortArray(value)[0]);
        } else {
          ret.setValue(getValueAsShortArray(value));
        }
        break;

      case TangoConst.Tango_DEV_LONG:
        if(size==1) {
          ret.setValue(getValueAsLongArray(value)[0]);
        } else {
          ret.setValue(getValueAsLongArray(value));
        }
        break;

      case TangoConst.Tango_DEV_DOUBLE:
        if(size==1) {
          ret.setValue(getValueAsDoubleArray(value)[0]);
        } else {
          ret.setValue(getValueAsDoubleArray(value));
        }
        break;


    }

    return ret;

  }

  public double getValueAsDouble() throws DevFailed {

    Object[] value = getValueFromHardware();

    if(size!=1)
      Except.throw_exception(
              "error_read",
              "Cannot convert array to double",
              "TaggedAttribute.getValueAsDouble()");

    switch (type) {

      case TangoConst.Tango_DEV_BOOLEAN:
        return getValueAsBooleanArray(value)[0]?1:0;

      case TangoConst.Tango_DEV_UCHAR:
        return getValueAsCharArray(value)[0];

      case TangoConst.Tango_DEV_SHORT:
         return getValueAsShortArray(value)[0];

      case TangoConst.Tango_DEV_LONG:
        return getValueAsLongArray(value)[0];

      case TangoConst.Tango_DEV_DOUBLE:
        return getValueAsDoubleArray(value)[0];

    }

    return Double.NaN;

  }


  @Override
  public void setValue(AttributeValue attributeValue) throws DevFailed {

    try {

    switch (type) {

      case TangoConst.Tango_DEV_BOOLEAN: {
        boolean b = (Boolean)attributeValue.getValue();
        plc.write(tagName,b,1);
      }
      break;
      case TangoConst.Tango_DEV_UCHAR: {
        byte b = (Byte)attributeValue.getValue();
        Character c = new Character((char)b);
        plc.write(tagName,c,1);
      }
      break;

      case TangoConst.Tango_DEV_SHORT: {
        short s = (Short)attributeValue.getValue();
        plc.write(tagName,s,1);
      }
      break;

      case TangoConst.Tango_DEV_LONG: {
        int i = (Integer)attributeValue.getValue();
        plc.write(tagName,i,1);
      }
      break;

      case TangoConst.Tango_DEV_DOUBLE:{
        double d = (Double)attributeValue.getValue();
        Float f = new Float(d);
        plc.write(tagName,f,1);
      }
      break;

    }

    } catch (EthernetIPException e) {
      Except.throw_exception(
          "error_read",
          "Reading error " + e.getMessage(),
          "TaggedAttribute.getValue()"
      );
    }

  }

  @Override
  public StateMachineBehavior getStateMachine() throws DevFailed {
    return null;
  }


  private String getCIPTypeName(Object o) {

    if(o instanceof Boolean)
      return "BOOL";
    else if (o instanceof Character)
      return "SINT";
    else if (o instanceof Short)
      return "INT";
    else if (o instanceof Integer)
      return "DINT";
    else if (o instanceof Float)
      return "REAL";
    else
      return "UNKNOWN";

  }

  // Boolean BOOL Tango_DEV_BOOLEAN
  private boolean[] getValueAsBooleanArray(Object[] value) throws DevFailed {

    boolean[] ret = new boolean[value.length];

    for(int i=0;i<ret.length;i++) {

      Object o = value[i];

      if(o instanceof Boolean) {
        ret[i] = ((Boolean)o).booleanValue();
      } else {
        // not supported type
        Except.throw_exception(
            "error_read",
            "Unexpected CIP type, BOOL expected, got " + getCIPTypeName(o),
            "TaggedAttribute.getValue()"
        );
      }
    }

    return ret;

  }

  // Byte SINT TangoConst.Tango_DEV_UCHAR
  private byte[] getValueAsCharArray(Object[] value) throws DevFailed {

    byte[] ret = new byte[value.length];

    for(int i=0;i<ret.length;i++) {

      Object o = value[i];

      if(o instanceof Character) {
        ret[i] = (byte)((Character)o).charValue();
      } else {
        // not supported type
        Except.throw_exception(
            "error_read",
            "Unexpected CIP type, SINT expected, got " + getCIPTypeName(o),
            "TaggedAttribute.getValue()"
        );
      }
    }

    return ret;

  }

  // Short INT TangoConst.Tango_DEV_SHORT
  private short[] getValueAsShortArray(Object[] value) throws DevFailed {

    short[] ret = new short[value.length];

    for(int i=0;i<ret.length;i++) {

      Object o = value[i];

      if(o instanceof Short) {
        ret[i] = ((Short)o).shortValue();
      } if(o instanceof Character) {
        // Wrap Byte to short
        ret[i] = (short)((Character)o).charValue();
      } else {
        // not supported type
        Except.throw_exception(
            "error_read",
            "Unexpected CIP type, INT expected, got " + getCIPTypeName(o),
            "TaggedAttribute.getValue()"
        );
      }
    }

    return ret;

  }


  // Int DINT Tango_DEV_LONG
  private int[] getValueAsLongArray(Object[] value) throws DevFailed {

    int[] ret = new int[value.length];

    for(int i=0;i<ret.length;i++) {

      Object o = value[i];

      if(o instanceof Integer) {
        ret[i] = ((Integer)o).intValue();
      } else {
        // not supported type
        Except.throw_exception(
            "error_read",
            "Unexpected CIP type, DINT expected, got " + getCIPTypeName(o),
            "TaggedAttribute.getValue()"
        );
      }
    }

    return ret;

  }


  // Double REAL TangoConst.Tango_DEV_DOUBLE
  private double[] getValueAsDoubleArray(Object[] value) throws DevFailed {

    double[] ret = new double[value.length];
    for(int i=0;i<ret.length;i++) {

      Object o = value[i];

      if(o instanceof Float) {
        ret[i] =  (((Float)value[i]).floatValue()) * factor - startOffset;
      } else {
        // not supported type
        Except.throw_exception(
            "error_read",
            "Unexpected CIP type, REAL expected, got " + getCIPTypeName(o),
            "TaggedAttribute.getValue()"
        );
      }
    }

    return ret;

  }



}

