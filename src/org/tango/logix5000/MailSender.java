package org.tango.logix5000;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;

public class MailSender {


  final int COMM_TIMEOUT = 1000; // 1000msec timeout
  final Charset UTF8 = Charset.forName("UTF-8");

  private String SMTPServer;
  private String lastError;
  private Socket sock = null;
  private String subject;

  public MailSender(String SMTPServerName,String subject) {

    SMTPServer = SMTPServerName;
    lastError = "";
    this.subject = subject;

  }

  // ----------------------------------------------------------------------------

  private void close() {
    if (sock != null) {
      try {
        sock.close();
      } catch (IOException e) {
      }
      sock = null;
    }
  }

  // ----------------------------------------------------------------------------

  private String byteToStr(byte[] b, int length) {
    StringBuffer bf = new StringBuffer();
    for (int i = 0; i < length; i++)
      bf.append((char)b[i]);
    return bf.toString();
  }

  private String byteToStr(byte[] b) {
    return byteToStr(b, b.length);
  }

  private byte[] strToByte(String s) {
    return s.getBytes(UTF8);
  }

  // ----------------------------------------------------------------------------

  private boolean write(byte[] buffer) {

    try {
      sock.getOutputStream().write(buffer);
    } catch (IOException e) {
      close();
      lastError = e.getMessage();
      return false;
    }

    return true;

  }

  // ----------------------------------------------------------------------------

  boolean CheckResponse(String cmd, int expectedCode) {

    byte[] buffer = new byte[1024];
    int nbRead;

    try {
      nbRead = sock.getInputStream().read(buffer);
    } catch (IOException e) {
      lastError = e.getMessage();
      close();
      return false;
    }

    if (nbRead < 3) {
      close();
      lastError = "SMTP: Unexpected SMTP response";
      return false;
    }


    int retCode = Integer.parseInt(byteToStr(buffer, 3));

    if (retCode != expectedCode) {
      close();
      lastError = "SMTP: Unexpected SMTP response for " + cmd +
              ", got:" + byteToStr(buffer) + " ,expected:" + expectedCode;
      return false;
    }

    return true;

  }


  // ----------------------------------------------------------------------------
  boolean SendMessage(String destAdd, String fromAdd, String date, String message) {

    byte[] buffer;
    lastError = "None";

    // Build connection
    sock = new Socket();
    try {
      sock.connect(new InetSocketAddress(SMTPServer, 25),COMM_TIMEOUT);
    } catch (IOException e) {
      lastError = SMTPServer + ": " + e.getMessage();
      return false;
    }

    // Read header message
    if (!CheckResponse("HEADER", 220)) return false;

    // Send HELO message
    buffer = strToByte("HELO healarm\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("HELO", 250)) return false;

    // Send MAIL FROM
    buffer = strToByte("MAIL FROM: <" + fromAdd + ">\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("MAIL FROM", 250)) return false;

    // Send RCPT TO
    buffer = strToByte("RCPT TO: <" + destAdd + ">\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("RCPT TO", 250)) return false;

    // Send DATA
    buffer = strToByte("DATA\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("DATA", 354)) return false;

    // Send Message
    buffer = strToByte("Subject: " + subject + "\nFrom: HEALARM\nTo: " + destAdd + "\nDate: " + date + "\n\n" + message + "\n.\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("BODY", 250)) return false;

    // Send QUIT
    buffer = strToByte("QUIT\n");
    if( !write(buffer) ) return false;

    if (!CheckResponse("QUIT", 221)) return false;

    close();
    return true;

  }

  // ----------------------------------------------------------------------------
  String GetLastError() {
    return lastError;
  }

}
