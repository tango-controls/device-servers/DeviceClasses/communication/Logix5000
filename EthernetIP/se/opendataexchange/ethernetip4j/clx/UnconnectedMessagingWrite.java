package se.opendataexchange.ethernetip4j.clx;

import se.opendataexchange.ethernetip4j.EthernetIPException;

import java.nio.ByteBuffer;

public class UnconnectedMessagingWrite extends UnconnectedMessaging {
	public UnconnectedMessagingWrite(ByteBuffer tagName, Object value, long sessionHandle) throws EthernetIPException {
		super();
		request.asWriteRequestByteBuffer(tagName, sessionHandle, value, 1);		
	}
	
	public UnconnectedMessagingWrite(ByteBuffer tagName, Object value, long sessionHandle, int arraySize) throws EthernetIPException {
		super();
		request.asWriteRequestByteBuffer(tagName, sessionHandle, value, arraySize);
	}
}
