import se.opendataexchange.ethernetip4j.EthernetIPException;
import se.opendataexchange.ethernetip4j.clx.ControlLogixConnector;
import se.opendataexchange.ethernetip4j.clx.SimpleLogixCommunicator;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by PONS on 10/09/15.
 */
public class ABradley {

  static SimpleLogixCommunicator sComm=null;

  public static void test(String tagName,int size) {

    Object[] value=null;

    System.out.println("Reading " + tagName);

    try {
      value = (Object[])sComm.read(tagName,size);
    } catch(Exception e) {
      System.out.println("Read failed " + tagName + ":" + e.getMessage());
      e.printStackTrace();
    }

    for (int i = 0; i < value.length; i++) {

      Object o = value[i];

      if(o instanceof Float) {
        System.out.println("[#" + i + "]" + ((Float)value[i]).floatValue());
      } else if(o instanceof Character) {
        System.out.println("[#" + i + "]" + (int)(((Character)value[i]).charValue()));
      } else {
        System.out.println("[#" + i + "]" + value[i]);
      }

    }

  }

  public static void main(String arg[]) {

    String host = "160.103.105.74";
    int portNo = 44818;

    try {
      sComm = new SimpleLogixCommunicator(host, portNo);
    } catch(EthernetIPException e) {
      System.out.println("Connect failed "  + e.getMessage());
      System.exit(0);
    }

    System.out.println("Connection is OK !");

    test("Program:CTRM_Colors.ST_FLUIDS",1);
    test("Program:MSG_Flex_CTRM.HV_SYTU_HDB",6);
    test("ID22_data",10);

  }


}
