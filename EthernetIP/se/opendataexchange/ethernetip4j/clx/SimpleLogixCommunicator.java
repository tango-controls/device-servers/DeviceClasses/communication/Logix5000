package se.opendataexchange.ethernetip4j.clx;

import java.io.IOException;
import java.nio.*;
import java.util.ArrayList;

import se.opendataexchange.ethernetip4j.EthernetIPException;
import se.opendataexchange.ethernetip4j.EthernetIpBufferUtil;
import se.opendataexchange.ethernetip4j.EthernetIpDataTypeValidator;
import se.opendataexchange.ethernetip4j.services.UnconnectedMessageManagerRequest;
import se.opendataexchange.ethernetip4j.services.UnconnectedMessageManagerResponse;

/***
 * A class for communication with the plc system.
 */
public class SimpleLogixCommunicator extends ControlLogixConnector {

	EthernetIpBufferUtil incomingBuffer = new EthernetIpBufferUtil(1500);
	EthernetIpBufferUtil messageBuffer = new EthernetIpBufferUtil(1500);
	UnconnectedMessageManagerRequest request = new UnconnectedMessageManagerRequest(messageBuffer);
	UnconnectedMessageManagerResponse response = new UnconnectedMessageManagerResponse(incomingBuffer);
	public static final int MAX_WRITE_PAYLOAD = 212;
	
	/***
	 * Constructor.
	 * @param host Address of the control system.
	 * @param port Port to use (for instance 0xAF12)
	 * @throws IOException
	 */
	public SimpleLogixCommunicator(String host, int port) throws EthernetIPException {
		super(host, port);
	}

	/***
	 * Read values of the specified tag names from the plc.
	 * 
	 * This function can not handle segmented reads, all tag names/values must fit into one packet 
	 * 
	 * @param tagNames Array of tag names.
	 * @return Tag values.
	 * @throws EthernetIPException
	 */
	public Object[] read(String[] tagNames) throws EthernetIPException {

				Object[] objects = null;
				if (this.sessionHandle != 0) {
					//Send SendRRData request
					synchronized (socketChannel) {

            // Build tagNameBuffers
            ByteBuffer[] tagBuff = new ByteBuffer[tagNames.length];
            for(int i=0;i<tagBuff.length;i++)
              tagBuff[i] = buildTagNameBuffer(tagNames[i]);

						messageBuffer.getBuffer().clear();
						request.asReadRequestBuffer(tagBuff, this.sessionHandle);
						sendData(request.getByteBuffer());
						incomingBuffer.getBuffer().clear();
						receiveData(incomingBuffer.getBuffer());

					}
					//Check for response status
					//Extract object from response
					response.validate();
					objects = response.getValue();		
				}
				return objects;

			}

	/***
	 * Read a tag from the control system. 
	 * 
	 * Segmented transfer will occur if the value is too big to fit in a packet.
	 * 
	 * @param tagName Tag name
	 * @param arraySize Number of values (>1 if array).
	 * @return An array of tag values.
	 * @throws EthernetIPException
	 */
  public Object read(String tagName, int arraySize) throws EthernetIPException {


    ArrayList<Object> lst = new ArrayList<Object>();

    if (this.sessionHandle != 0) {

      Object object = null;

      // Build tagNamebuffer
      ByteBuffer tagBuff = buildTagNameBuffer(tagName);

      //Send SendRRData request
      request.asReadRequestBuffer(tagBuff, this.sessionHandle, arraySize);
      sendData(request.getByteBuffer());
      receiveData(incomingBuffer.getBuffer());
      //Check for response status
      //Extract object from response
      response.validate();
      object = response.getValues();
      if (!(object instanceof Object[])) { // arraySize == 1
        lst.add(object);
      } else {
        for (Object obj : (Object[]) object) {
          lst.add(obj);
        }
      }
      int offset = response.getPayloadSize();
      while (response.getGeneralStatus() == 0x06) {
        synchronized (socketChannel) {
          request.asReadRequestBuffer(tagBuff, this.sessionHandle, arraySize, offset);
          sendData(request.getByteBuffer());
          receiveData(incomingBuffer.getBuffer());
        }
        response.validate();
        object = response.getValues();
        for (Object obj : (Object[]) object) {
          lst.add(obj);
        }
        offset += response.getPayloadSize();
      }
    }
    Object[] objects = new Object[lst.size()];
    lst.toArray(objects);
    return objects;
  }

  /***
	 * Read a tag from the control system. 
	 * 
	 * Segmented transfer will occur if the value is too big to fit in a packet.
	 * 
	 * @param tagName Tag name.
	 * @param value Tag value(s) to write. Array of values if arraySize>1.
	 * @param arraySize Number of values (>1 if array).
	 * @throws EthernetIPException
	 */
	public void write(String tagName, Object value, int arraySize)
	throws EthernetIPException {

    ByteBuffer tagBuff = buildTagNameBuffer(tagName);

		//Send SendRRData request
		synchronized (socketChannel) {
			int totSize;
			int vSize;
			if (value instanceof Object[])
				vSize = EthernetIpDataTypeValidator.sizeOf(((Object[])value)[0]);
			else
				vSize = EthernetIpDataTypeValidator.sizeOf(value);
			totSize = arraySize * vSize;
			if (totSize < MAX_WRITE_PAYLOAD){ // Normal write
				request.asWriteRequestByteBuffer(tagBuff, this.sessionHandle, value, arraySize);
				this.sendData(request.getByteBuffer());
				try{ Thread.sleep(5); }catch(InterruptedException e){}
				super.receiveData(incomingBuffer.getBuffer());
				response.validate();
			}else{ // fragmentation needed
				int dataOffset = 0;
				int nbrWritten = 0;
				int writeCount;
				while (nbrWritten < arraySize){
					writeCount = Math.min(MAX_WRITE_PAYLOAD/vSize, arraySize-nbrWritten); // Max in 1 packet or all values left
					request.asWriteRequestByteBuffer(tagBuff, this.sessionHandle, value, arraySize, dataOffset, writeCount);
					this.sendData(request.getByteBuffer());
					super.receiveData(incomingBuffer.getBuffer());
					response.validate();
					nbrWritten += writeCount;
					dataOffset += vSize*writeCount;
				}
			}
		}				
	}
	
	/***
	 * Reads a tag value from the controller (if an array is specified, the first element will be read).
	 * 
	 * @param tagName Tag name.
	 * @return Tag value.
	 * @throws EthernetIPException
	 */
	public Object read(String tagName) throws EthernetIPException {
		Object[] objs = (Object[]) this.read(tagName,1);
		return objs[0];
	}


  private ByteBuffer buildTagNameBuffer(String tagName) throws EthernetIPException {

    if(tagName.indexOf("[")!=-1) {
      // We have an array item or range
      throw new EthernetIPException("Array item not supported");
    }

    String[] names = tagName.split("\\.");

    int length = 0;
    for(int i=0;i<names.length;i++) {
      length += names[i].length()+2;
      // Padding
      if(names[i].length()%2==1) length++;
    }

    ByteBuffer ret =  ByteBuffer.allocate(length);

    for(int i=0;i<names.length;i++) {
      ret.put((byte)0x91);
      ret.put((byte)names[i].length());
      ret.put(names[i].getBytes());
      // Padding
      if(names[i].length()%2==1)
        ret.put((byte)0);
    }

    ret.position(0);
    return ret;

  }


}
