package se.opendataexchange.ethernetip4j;

public class EthernetIPException extends Exception {

  public EthernetIPException(String message) {
    super(message);
  }

}
