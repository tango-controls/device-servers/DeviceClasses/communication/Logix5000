package org.tango.logix5000;


import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.command.CommandConfiguration;
import org.tango.server.command.ICommandBehavior;

import static org.tango.command.CommandTangoType.VOID;

public class ResetCommand implements ICommandBehavior {

  private TaggedAttribute attr;
  private String name;

  public ResetCommand(TaggedAttribute ta,String commandName) {
    attr = ta;
    name = commandName;
  }

  @Override
  public CommandConfiguration getConfiguration() throws DevFailed {
    CommandConfiguration cfg = new CommandConfiguration();
    cfg.setName(name);
    cfg.setDispLevel(DispLevel.OPERATOR);
    cfg.setInTangoType(VOID.getTangoIDLType());
    cfg.setOutTangoType(VOID.getTangoIDLType());
    return cfg;
  }

  @Override
  public Object execute(Object o) throws DevFailed {
    attr.reset();
    return null;
  }

  @Override
  public StateMachineBehavior getStateMachine() throws DevFailed {
    return null;
  }

}
