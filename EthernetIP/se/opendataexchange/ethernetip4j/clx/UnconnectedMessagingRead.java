package se.opendataexchange.ethernetip4j.clx;

import se.opendataexchange.ethernetip4j.EthernetIPException;

import java.nio.ByteBuffer;

public class UnconnectedMessagingRead extends UnconnectedMessaging{
	public UnconnectedMessagingRead(ByteBuffer tagName, long sessionHandle) throws EthernetIPException {
		super();
		request.asReadRequestBuffer(tagName, sessionHandle, 1);
	}
	
	public UnconnectedMessagingRead(ByteBuffer[] tagNames, long sessionHandle) throws EthernetIPException {
		super();
		request.asReadRequestBuffer(tagNames, sessionHandle);
	}
	
	public UnconnectedMessagingRead(ByteBuffer tagName,int arraySize, long sessionHandle) throws EthernetIPException {
		super();
		request.asReadRequestBuffer(tagName, sessionHandle, arraySize);
	}
	
	/***
	 * 
	 * @return Tag values from response
	 */
	public Object[] getValue() throws EthernetIPException {
		response.validate();
		return response.getValue();
	}
	
	public Object getValues() throws EthernetIPException {
		response.validate();
		return response.getValues();
	}
}
