package se.opendataexchange.ethernetip4j.segments;

import se.opendataexchange.ethernetip4j.EthernetIPException;
import se.opendataexchange.ethernetip4j.EthernetIpBufferUtil;
import se.opendataexchange.ethernetip4j.EthernetIpDataTypeValidator;
import se.opendataexchange.ethernetip4j.services.CipCommandServices;

import java.nio.ByteBuffer;

/**
 * 
 * <table border="1">
 * <tr><th>Byte</th><th>		Name</th><th>					Type</th><th>							Description</th></tr>
 * <tr><td>0</td><td>			Service</td><td> 				USINT</td><td>							{@link CipCommandServices}</td></tr>
 * <tr><td>1</td><td>			Request path size</td><td>	 	USINT</td><td> 							Tag name length (+1 if odd)</td></tr>
 * <tr><td>2 - x</td><td>		EPATH</td><td> 					Byte[]</td><td> 						Tag name (special if structure)</td></tr>
 * <tr><td>(x+1) - y</td><td>	Request data</td><td> 			Byte[]</td><td>							Depends on service</td></tr>
 * </table>
 * 
 */
public class CipMessageRouterRequest {
	private static int DEFAULT_OFFSET = 
		EthernetIpEncapsulationHeader.SEGMENT_LENGTH + 
		EthernetIpCommandSpecificData.SEGMENT_LENGTH +
		EthernetIpItemStruct.SEGMENT_LENGTH +
		CipPacketRequest.SEGMENT_LENGTH +
		CipCommandSpecificDataRequest.SEGMENT_DATA_OFFSET;

	EthernetIpBufferUtil buffer;

	/***
	 * Fills input buffer. Uses default offset.
	 * @param serviceId
	 * @param tagName
	 * @param value
	 * @param arraySize
	 * @param messageBuffer
	 * @return Segment length.
	 * @throws EthernetIPException
	 */
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, EthernetIpBufferUtil messageBuffer) throws EthernetIPException {
		return fillBuffer(serviceId, tagName, value, arraySize, messageBuffer, DEFAULT_OFFSET);
	}
	
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, EthernetIpBufferUtil messageBuffer, int offset) throws EthernetIPException {
		int segmentLength = getSegmentLength(serviceId, tagName, value, arraySize);
		setService(messageBuffer, offset, (short)serviceId);
		setRequestPathSize(messageBuffer, offset, serviceId, (short)tagName.capacity());
		setEPATH(messageBuffer, offset, tagName);
		setRequestData(messageBuffer, offset, serviceId, tagName, value, arraySize);
		return segmentLength;
	}
	
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, int dataOffset, EthernetIpBufferUtil messageBuffer) throws EthernetIPException {
		return fillBuffer(serviceId, tagName, value, arraySize, dataOffset, messageBuffer, DEFAULT_OFFSET);
	}
	
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, int dataOffset, EthernetIpBufferUtil messageBuffer, int offset) throws EthernetIPException {
		int segmentLength = getSegmentLength(serviceId, tagName, value, arraySize);
		setService(messageBuffer, offset, (short)serviceId);
		setRequestPathSize(messageBuffer, offset, serviceId, (short)tagName.capacity());
		setEPATH(messageBuffer, offset, tagName);
		setRequestData(messageBuffer, offset, serviceId, tagName, value, arraySize, dataOffset);
		return segmentLength;
	}
	
	/***
	 * 
	 * @param serviceId
	 * @param tagName
	 * @param value
	 * @param arraySize
	 * @param dataOffset
	 * @param writeCount when writing segmented arrays, this is how many values from the array to be put into the message
	 * @param messageBuffer
	 * @return CIP message length (segment length).
	 * @throws EthernetIPException
	 */
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, int dataOffset, int writeCount, EthernetIpBufferUtil messageBuffer) throws EthernetIPException {
		return fillBuffer(serviceId, tagName, value, arraySize, dataOffset, writeCount, messageBuffer, DEFAULT_OFFSET);
	}
	
	public static int fillBuffer(int serviceId, ByteBuffer tagName, Object value,int arraySize, int dataOffset, int writeCount, EthernetIpBufferUtil messageBuffer, int offset) throws EthernetIPException {
		int segmentLength = getSegmentLength(serviceId, tagName, value, writeCount);
		setService(messageBuffer, offset, (short)serviceId);
		setRequestPathSize(messageBuffer, offset, serviceId, (short)tagName.capacity());
		setEPATH(messageBuffer, offset, tagName);
		setRequestData(messageBuffer, offset, serviceId, tagName, value, arraySize, dataOffset, writeCount);
		return segmentLength;
	}
	
	/*** Region: Getters and setters ***/
	
	public static short getService(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getUSINT(0 + offset);
	}
		
	public static void setService(EthernetIpBufferUtil buffer, int offset, short value) {
		buffer.putUSINT(0 + offset, value);
	}
	
	public static short getRequestPathSize(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getUSINT(1 + offset);
	}
	
	public static void setRequestPathSize(EthernetIpBufferUtil buffer, int offset, int serviceId, short value) {
  	buffer.putByte(1 + offset, (byte)(value/2));
	}
	
	public static short getExtendedSymbolSegment(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getByte(2 + offset);
	}
	
	public static void setExtendedSymbolSegment(EthernetIpBufferUtil buffer, int offset, short value) {
		buffer.putByte(2 + offset,(byte) value);
	}
	
	public static short getRequestDataSize(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getByte(3 + offset);
	}
	
	public static void setRequestDataSize(EthernetIpBufferUtil buffer, int offset, short value) {
		buffer.putByte(3 + offset,(byte) value);
	}
	
	public static byte[] getRequestTagName(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getByteArray(4 + offset, getRequestDataSize(buffer, offset));
	}
	
	public static void setEPATH(EthernetIpBufferUtil buffer, int offset, ByteBuffer tagName) throws EthernetIPException {
		buffer.putByteArray(2 + offset, tagName.array());
	}
	
	public static byte[] getRequestData(EthernetIpBufferUtil buffer, int offset) {
		return buffer.getByteArray(4 + offset, getRequestDataSize(buffer, offset));
	}
	
	public static void setRequestData(EthernetIpBufferUtil buffer, int offset, int serviceId, ByteBuffer tagName, Object value,int arraySize) throws EthernetIPException {
		int tmpOffset = 2 + tagName.capacity() + offset;
		if(serviceId == CipCommandServices.CIP_READ_DATA){
			buffer.putUINT(0 + tmpOffset, arraySize);
		} else if(serviceId == CipCommandServices.CIP_WRITE_DATA){
			buffer.putByte(0 + tmpOffset, EthernetIpDataTypeValidator.getType(value));
			buffer.putUINT(2 + tmpOffset, arraySize);
			if (arraySize > 1)
				EthernetIpDataTypeValidator.putValues(value, buffer, 4 + tmpOffset, arraySize, 0);
			else
				EthernetIpDataTypeValidator.putValue(value, buffer, 4 + tmpOffset);
			
		}else{
			throw new EthernetIPException("Request service not implemented");
		}
	}
	
	public static void setRequestData(EthernetIpBufferUtil buffer, int offset, int serviceId, ByteBuffer tagName, Object value,int arraySize,int dataOffset) throws EthernetIPException {
		int tmpOffset = 2 + tagName.capacity() + offset;
		if(serviceId == CipCommandServices.CIP_READ_FRAGMENT){
			buffer.putUINT(0 + tmpOffset, arraySize);
			buffer.putUDINT(2 + tmpOffset, dataOffset);			
		} else{
			throw new EthernetIPException("Request service not implemented");
		}
	}
	
	public static void setRequestData(EthernetIpBufferUtil buffer, int offset, int serviceId, ByteBuffer tagName, Object value,int arraySize,int dataOffset, int writeCount) throws EthernetIPException {
		int tmpOffset = 2 + tagName.capacity() + offset;
		if (serviceId == CipCommandServices.CIP_WRITE_FRAGMENT){
			buffer.putByte(0 + tmpOffset, EthernetIpDataTypeValidator.getType(value));
			buffer.putUINT(2 + tmpOffset, arraySize);
			buffer.putUINT(4 + tmpOffset, dataOffset);
			if (arraySize > 1)
				EthernetIpDataTypeValidator.putValues(value, buffer, 8 + tmpOffset, writeCount, dataOffset);
			else
				EthernetIpDataTypeValidator.putValue(value, buffer, 8 + tmpOffset);			
		} else{
			throw new EthernetIPException("Request service not implemented");
		}
	}

	/***
	 * Get length of CIP request segment.
	 * 
	 * @param serviceId
	 * @param tagName
	 * @param value Value to write. Null in case of read requests.
	 * @param writeCount How many values to write. 0 in case of read requests.
	 * @return CIP message length (segment length).
	 * @throws EthernetIPException
	 */
	public static int getSegmentLength(int serviceId, ByteBuffer tagName, Object value, int writeCount) throws EthernetIPException {

		int length = 0;
		if(serviceId == CipCommandServices.CIP_READ_DATA){

			length = 4+tagName.capacity();

		} else if(serviceId == CipCommandServices.CIP_WRITE_DATA || serviceId == CipCommandServices.CIP_WRITE_FRAGMENT){

			length = 6+tagName.capacity();

			// Add data value size
			length += EthernetIpDataTypeValidator.sizeOf(value)*writeCount;
			if (serviceId == CipCommandServices.CIP_WRITE_FRAGMENT)
				length += 4; // For the data offset
			if (length%2==1) length++;
			
		}else if(serviceId == CipCommandServices.CIP_READ_FRAGMENT){

  		length = 8+tagName.capacity();

		}
		return length;
	}
	
}
